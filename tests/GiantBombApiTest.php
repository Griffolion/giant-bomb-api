<?php

namespace Griffolion\GiantBombApi\Tests;

use Griffolion\GiantBombApi\GiantBombApi;
use Griffolion\GiantBombApi\GiantBombApiFactory;
use Griffolion\GiantBombApi\Resources;
use PHPUnit\Framework\TestCase;

/**
 * Class GiantBombApiTest
 * @package Griffolion\GiantBombApi\Tests
 */
class GiantBombApiTest extends TestCase
{
    /** @var GiantBombApi */
    private $giantBombApi;

    public function setUp() {
        parent::setUp();
        $this->giantBombApi = GiantBombApiFactory::createApi("teststring");
    }

    public function tearDown() {
        parent::tearDown();
    }

    public function testSearch() {
        $search = $this->giantBombApi->search([Resources::CHARACTER], "Sephiroth");
        $this->assertInternalType('string', gettype($search), "Expected returned data to be of type string, got " . gettype($search) . ".");
    }

    public function testGet() {
        $get = $this->giantBombApi->get(Resources::CHARACTER, "", ['name']);
        $this->assertInternalType('string', gettype($get), "Expected returned data to be of type string, got " . gettype($get) . ".");
    }
}