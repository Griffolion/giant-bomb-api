<?php

namespace Griffolion\GiantBombApi\Tests;

require_once __DIR__."/../vendor/autoload.php";

use Griffolion\GiantBombApi\GiantBombApi;
use Griffolion\GiantBombApi\GiantBombApiFactory;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

/**
 * Class GiantBombApiFactoryTest
 * @package Griffolion\GiantBombApi\Tests
 */
class GiantBombApiFactoryTest extends TestCase
{
    public function testFactoryMethod() {
        $testApi = GiantBombApiFactory::createApi('testkey');
        $reflect = new ReflectionClass($testApi);
        $this->assertEquals(GiantBombApi::class, $reflect->getName(), "Expected the returned class from factory method to be an instance of GiantBombApi, instead got " . $reflect->getName() . ".");
    }
}