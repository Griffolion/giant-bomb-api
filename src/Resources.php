<?php

namespace Griffolion\GiantBombApi;

/**
 * Class Resources
 * @package Griffolion\GiantBombApi
 */
class Resources {

    const ACCESSORY =       'accessory';
    const ACCESSORIES =     'accessories';
    const CHARACTER =       'character';
    const CHARACTERS =      'characters';
    const CHAT =            'chat';
    const CHATS =           'chats';
    const COMPANY =         'company';
    const COMPANIES =       'companies';
    const CONCEPT =         'concept';
    const CONCEPTS =        'concepts';
    const DLC =             'dlc';
    const DLCS =            'dlcs';
    const FRANCHISE =       'franchise';
    const FRANCHISES =      'franchises';
    const GAME =            'game';
    const GAMES =           'games';
    const GAME_RATING =     'game_rating';
    const GAME_RATINGS =    'game_ratings';
    const GENRE =           'genre';
    const GENRES =          'genres';
    const LOCATION =        'location';
    const LOCATIONS =       'locations';
    const OBJECT =          'object';
    const OBJECTS =         'objects';
    const PERSON =          'person';
    const PEOPLE =          'people';
    const PLATFORM =        'platform';
    const PLATFORMS =       'platforms';
    const PROMO =           'promo';
    const PROMOS =          'promos';
    const RATING_BOARD =    'rating_board';
    const RATING_BOARDS =   'rating_boards';
    const REGION =          'region';
    const REGIONS =         'regions';
    const RELEASE =         'release';
    const RELEASES =        'releases';
    const REVIEW =          'review';
    const REVIEWS =         'reviews';
    const SEARCH =          'search';
    const THEME =           'theme';
    const THEMES =          'themes';
    const TYPES =           'types';
    const USER_REVIEW =     'user_review';
    const USER_REVIEWS =    'user_reviews';
    const VIDEO =           'video';
    const VIDEOS =          'videos';
    const VIDEO_CATEGORY =  'video_category';
    const VIDEO_CATEGORIES ='video_categories';
    const VIDEO_SHOW =      'video_show';
    const VIDEO_SHOWS =     'video_shows';
}