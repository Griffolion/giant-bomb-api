<?php

namespace Griffolion\GiantBombApi;

/**
 * Class ResponseFormats
 * @package Griffolion\GiantBombApi
 */
class ResponseFormats {

    CONST JSON = 'json';
    CONST XML = 'xml';
}