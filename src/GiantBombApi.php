<?php

namespace Griffolion\GiantBombApi;

/**
 * Class GiantBombApi
 * @package Griffolion\GiantBombApi
 */
class GiantBombApi {

    const BASE_URL = "http://www.giantbomb.com/api";

    /** @var string */
    private $apiKey;

    /**
     * GiantBombApi constructor.
     *
     * @param string $apiKey
     */
    public function __construct($apiKey) {
        $this->setApiKey($apiKey);
    }

    /**
     * Gets a resource from the API according to the given parameters.
     *
     * @param string $resourceType
     * @param string $resourceId
     * @param array $fieldList
     * @param string $format
     * @return string|false
     */
    public function get($resourceType, $resourceId, array $fieldList, $format = ResponseFormats::JSON) {
        $url = self::BASE_URL . "/$resourceType/$resourceId?api_key={$this->apiKey}&format=$format&field_list=" . implode(',', $fieldList);
        $curlHandle = $this->initializeCurlHandle($url);
        if ($curlHandle === false) {
            return false;
        }
        $response = curl_exec($curlHandle);
        curl_close($curlHandle);
        return $response;
    }

    /**
     * Searches the API according to the given query parameters.
     *
     * @param array $resources
     * @param string $query
     * @param string $format
     * @return string|false
     */
    public function search(array $resources, $query = '', $format = ResponseFormats::JSON) {
        $url = self::BASE_URL . "/search?api_key={$this->apiKey}&format=$format&query=$query&resources=" . implode(',', $resources);
        $curlHandle = $this->initializeCurlHandle($url);
        if ($curlHandle === false) {
            return false;
        }
        $response = curl_exec($curlHandle);
        curl_close($curlHandle);
        return $response;
    }

    /**
     * Initializes and returns a new Curl Handle Resource or false on error.
     *
     * @param string $url
     * @return resource|bool
     */
    private function initializeCurlHandle($url) {
        $curlHandle = curl_init($url);
        if ($curlHandle === false) {
            return false;
        }
        curl_setopt($curlHandle, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_USERAGENT, 'Giant Bomb API Library For PHP');
        return $curlHandle;
    }

    /**
     * Returns the API Key.
     *
     * @return string
     */
    public function getApiKey() {
        return $this->apiKey;
    }

    /**
     * Sets the API Key.
     *
     * @param string $apiKey
     */
    public function setApiKey($apiKey) {
        $this->apiKey = $apiKey;
    }
}