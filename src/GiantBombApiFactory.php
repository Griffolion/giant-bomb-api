<?php

namespace Griffolion\GiantBombApi;

/**
 * Class GiantBombApiFactory
 * @package Griffolion\GiantBombApi
 */
class GiantBombApiFactory {

    /**
     * Returns a new GiantBombApi class
     *
     * @param string $apiKey
     * @return GiantBombApi
     */
    public static function createApi($apiKey) {
        return new GiantBombApi($apiKey);
    }
}